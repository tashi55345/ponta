from bs4 import BeautifulSoup
import lxml.html
from selenium import webdriver
from selenium.webdriver.common.keys import Keys as keys

""" Ponta web 自動ログインをしポンタポイントを取得し表示する """
def main():
    driver = webdriver.PhantomJS()
    driver.get('https://point.recruit.co.jp/point/balanceRef')
    """ 自動でメルアドとパスワード入力しエンターキーを押す """
    uid = driver.find_element_by_name("mainEmail")
    password = driver.find_element_by_name("passwd")
    uid.send_keys("")
    password.send_keys("" + keys.RETURN)
    """ ログイン後のページを読み込みポンタポイントを取得し表示 """
    data = driver.page_source.encode('utf-8')
    soup = BeautifulSoup(data, "lxml")
    point = soup.find('span', class_='pontaPoint').text
    print("Pontaポイント: " + point + "ポイント")

if __name__ == '__main__':
    main()
